# Create-OPC-UA-server
 A comprehensive guide and codebase for building your own OPC-UA server.
I followed several tutorials, in particular, Macheronte (no longer available) and [Industry4.0tv](https://www.youtube.com/watch?v=gxA7SDNLHgc&t=1804s&ab_channel=Industry40tv).

## Prerequisites
Read at least one time [Information Model Guide](https://reference.opcfoundation.org/Core/Part5/v104/docs/)

## Build Information Model
Especially if you're at the first time with OPC-UA, writing an information model in a solid way could be hard, my advice is to rely on  [UaModeler]([http://a.com](https://www.unified-automation.com/products/development-tools/uamodeler.html)) which not only simplifies the modeling of information itself, this tool generates the source code required to implement the designed model. If you need more than 10 nodes it'll require a license, but in any case, I found it very useful. We'll use it just for the XML file.

1. File-> New Project-> Modeling -> Select the first two models (remember to choose the output directory)
2. Go to Information Model (on the left) and Add the nodes you need: Click + and choose from 
3. Export XML: Click on Folder with project name -> Models-> right click on blue wheel->Export XML
4. You should be able to visualize XML schema, now you can rename the Nodes with a more explicit name rather than a number

## Model Compiler
-At 11:42[Industry4.0tv](https://www.youtube.com/watch?v=gxA7SDNLHgc&t=1804s&ab_channel=Industry40tv) explains how to use [Model Compiler](https://github.com/OPCFoundation/UA-ModelCompiler) follow his steps
- Launch this inside of the modelcompiler.exe Folder.
- Example: \Opc.Ua.ModelCompiler compile -d2 "C:\*\tutorial\exampleserver.xml" -cg "C:\Users\*\exampleserver.csv" -o2 "C:\*\tutorial" -version v104
- Now copy and paste all your files inside a C# new Project

##  Creating OPCUA Server Using .Net Standard SDK
-You will need a File.config where you can specify some parameters of your OPC Server (e.g. Security parameters).
